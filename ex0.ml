open Printf
module List = ListLabels

let zoom_level = ref 1.5

let deja_vu_sans_mono =
  lazy begin
    let length =
      List.fold_left ~f:(fun c s -> c + String.length s + 1) ~init:0
        Deja_vu_sans_mono.ttf in
    let buffer =
      Bigarray.Array1.create Bigarray.int8_unsigned Bigarray.c_layout length in
    let index = ref 0 in
    List.iter Deja_vu_sans_mono.ttf ~f:(fun s ->
        String.iter (fun c ->
            Bigarray.Array1.set buffer !index (int_of_char c);
            incr index;
          ) s;
        Bigarray.Array1.set buffer !index (int_of_char '\n');
        incr index;
      );
    let offset = List.hd (Stb_truetype.enum buffer) in
    begin match Stb_truetype.init buffer offset with
    | None -> assert false
    | Some font -> font
    end
  end

let render ~w ~h ~mouse_x ~mouse_y context t =
  let vg_task = Wall_canvas.new_frame context in
  (* Some kind of frame rectangle resize-friendly: *)
  Wall_canvas.draw' vg_task Wall.Transform.identity
    (Wall.Paint.rgba 0. 0.7 0. 0.3)
    begin Wall_canvas.stroke_path
        Wall.Outline.{ default with stroke_width = !zoom_level *. 5. }
      @@ fun t ->
      Wall_canvas.Path.rect t ~x:(10.) ~y:(10.)
        ~w:(float w -. 20.) ~h:(float h -. 20.);
    end;
  (* Diamond cursor: *)
  Wall_canvas.draw' vg_task
    Wall.Transform.(
      identity
      |> translate ~x:(float mouse_x) ~y:(float mouse_y)
      |> rotate (Wall_canvas.pi /. 4.)
      |> rescale ~sx:!zoom_level ~sy:!zoom_level
    )
    (Wall.Paint.rgba 1.0 0. 0. 0.3)
    begin Wall_canvas.fill_path @@ fun t ->
      let wo2 = 10. in
      Wall_canvas.Path.rect t ~x:(0. -. wo2) ~y:(0. -. wo2)
        ~w:(2. *. wo2) ~h:(2. *. wo2)
    end;
  (*
     Some Text:
   *)
  Wall_canvas.text' vg_task Wall.Transform.identity
    (Wall.Paint.rgba 0. 0. 1. 0.7)
    (Wall.Font.make
       ~spacing:(1.)
       ~blur:0. ~size:(!zoom_level *. 30.0) (Lazy.force deja_vu_sans_mono))
    ~x:30. ~y:(float h -. 40.) (sprintf "Some text … %f" t);

  (* An animation: *)
  let progress = int_of_float (10. *. t) mod 20 |> float in
  Wall_canvas.draw' vg_task
    Wall.Transform.(
      identity
      |> translate ~x:(100. +. (progress *. 10.) ) ~y:(40.)
      |> rescale ~sx:!zoom_level ~sy:!zoom_level
    )
    (Wall.Paint.radial_gradient
       ~cx:10. ~cy:10.
       ~inr:(-10.) ~outr:10.
       ~inner:(Wall.Color.v 1.0 0. 0. 0.5)
       ~outer:(Wall.Color.gray ~a:0.375 0.0))
    begin Wall_canvas.fill_path
      @@ fun t ->
      Wall_canvas.Path.ellipse t ~cx:(10.) ~cy:(10.)
        ~rx:(40.) ~ry:(20.);
    end;

  Wall_canvas.flush_frame context (Gg.V2.v (float w) (float h));
  ()


open Tsdl
open Tgles2

let main () =
  let fw = 600 in
  let fh = 400 in
  printf "Let's Go!\n%!";
  Printexc.record_backtrace true;
  match Sdl.init Sdl.Init.video with
  | Error (`Msg e) -> Sdl.log "Init error: %s" e; exit 1
  | Ok () ->
    match Sdl.create_window ~w:fw ~h:fh "WALL Test"
            Sdl.Window.(windowed + opengl + resizable) with
    | Error (`Msg e) -> Sdl.log "Create window error: %s" e; exit 1
    | Ok window ->
      (*Sdl.gl_set_attribute Sdl.Gl.context_profile_mask Sdl.Gl.context_profile_core;*)
      (*Sdl.gl_set_attribute Sdl.Gl.context_major_version 2;*)
      (*Sdl.gl_set_attribute Sdl.Gl.context_minor_version 1;*)
      ignore (Sdl.gl_set_attribute Sdl.Gl.stencil_size 1);
      match Sdl.gl_create_context window with
      | Error (`Msg e) -> Sdl.log "Create context error: %s" e; exit 1
      | Ok ctx ->
        let context = Wall_canvas.create_gl ~antialias:false in
        let t = ref 0.0 in
        let quit = ref false in
        let event = Sdl.Event.create () in
        while not !quit do
          while Sdl.poll_event (Some event) do
            match Sdl.Event.enum (Sdl.Event.get event Sdl.Event.typ) with
            | `Quit -> quit := true
            | `Key_up ->
              let key_scancode e = Sdl.Scancode.enum Sdl.Event.(get e keyboard_scancode) in
              begin match key_scancode event with
              | `Q -> printf "Q key-up!\n%!"; quit := true
              | `Equals -> zoom_level := 1.1 *. !zoom_level
              | `Minus -> zoom_level := 0.9 *. !zoom_level
              | _ -> ()
              end
            | _ -> ()
          done;
          Unix.sleepf 0.020;
          t := !t +. 0.050;
          let w, h = Sdl.get_window_size window in
          Gl.viewport 0 0 w h;
          Gl.clear_color 0.3 0.3 0.32 1.0;
          Gl.(clear (color_buffer_bit lor depth_buffer_bit lor stencil_buffer_bit));
          Gl.enable Gl.blend;
          Gl.blend_func_separate Gl.one Gl.src_alpha Gl.one Gl.one_minus_src_alpha;
          Gl.enable Gl.cull_face_enum;
          Gl.disable Gl.depth_test;
          let _, (mouse_x, mouse_y) = Sdl.get_mouse_state () in
          render ~w ~h ~mouse_x ~mouse_y context !t;
          Sdl.gl_swap_window window;
        done;
        Sdl.gl_delete_context ctx;
        Sdl.destroy_window window;
        Sdl.quit ();
        printf "Done.\n%!";
        exit 0

let () = main ()