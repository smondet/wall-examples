

build () {
    rm 0th-example
    ocamlfind opt -g -linkpkg -package grenier.binpacking,stb_truetype,wall,tsdl deja_vu_sans_mono.ml ex0.ml -o 0th-example
    cat > .merlin <<EOF
S .
B .
PKG grenier.binpacking
PKG stb_truetype
PKG wall
PKG tsdl
EOF
    # ocamlfind opt -g -linkpkg -package grenier.binpacking,stb_truetype,wall,tsdl example.ml -o repo-example
}
view () {
    0th-example
}

if [ "$1" = "" ] ; then
    build
else
    $*
fi
